package marsh.stt;

import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.event.player.AttackBlockCallback;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SaveTheTrees implements ModInitializer {
	public static final String MOD_ID = "marsh.savethetrees";
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	private final List<String> _protectedBlocks = Arrays.asList("block.minecraft.oak_log", "block.minecraft.spruce_log", "block.minecraft.birch_log", "block.minecraft.acacia_log", "block.minecraft.mangrove_log", "block.minecraft.cherry_log");

	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.
		AttackBlockCallback.EVENT.register(this::OnAttackBlock);
	}

	private ActionResult OnAttackBlock(PlayerEntity player, World world, Hand hand, BlockPos pos, Direction direction) {
		if (world.isClient() | player.isSpectator())
			return ActionResult.PASS;

		BlockState state = world.getBlockState(pos);
		var nameOfBlockHit = state.getBlock().asItem().getTranslationKey();

		if (_protectedBlocks.contains(nameOfBlockHit)) {
			player.sendMessage(Text.of("Please stop killing the trees"));
			LOGGER.info("Told player to stop killing trees");
			return ActionResult.FAIL;
		}

		return ActionResult.PASS;
	}
}